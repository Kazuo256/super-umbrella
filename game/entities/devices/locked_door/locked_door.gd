extends StaticBody2D

export(int) var keys = 1
onready var ani_player = $Sprite.get_node("AnimationPlayer")

func _ready():
	#Makes lock and remaining scale fixed:
	var scale = self.get_scale()
	$Sprite.set_scale(Vector2(0.9/scale.x,0.9/scale.y))
	$Remaining.set_scale(Vector2(1/scale.x,1/scale.y))
	
	$Sprite.get_node("Needed").set_text(String(keys))
	

func open(Collector):
	if Collector.keys >= keys:
		Collector.keys -= keys

		#Hide Door
		$Sprite.hide()
		$door_shape.queue_free()

		#Show remaining keys
		$Remaining.set_text(String(Collector.keys))
		$Timer.start()
	else:
		if not ani_player.is_playing():
			ani_player.play("denied")

func _on_interactable_area_body_entered(body):
	if body.is_in_group("Player"):
		open(body.get_node("Collector"))


func _on_Timer_timeout():
	self.queue_free()
