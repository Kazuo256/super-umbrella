extends Position2D

export(PackedScene) var Entity

#location of stage/Entities
var Entities

func _ready():
	#Let's hope nobody forgot to set an entity to spawn
	assert(not Entity == null)
	
	var finder = preload("res://managers/finder/Finder.gd").new(get_tree())
	
	assert(finder.has_entity(""))
	Entities = finder.get_entity("")

func _spawn():
	var new = Entity.instance()
	new.position = self.position
	new.rotation = self.rotation
	Entities.add_child(new)
