extends Node

const PHYSICS_BASE = preload("res://entities/components/platformer/PhysicsBase.gd")

var _active_state = null

# TODO where is this used?
func switch_state(new_state):
	_active_state = self.get_node(new_state)

func compute_movement(current_velocity, control, delta):
	
	if _active_state == null:
		return current_velocity
	
	var velocity = current_velocity
	
	# Limit horizontal movement
	velocity.x += _active_state.acceleration * control.action_direction.x
	velocity.x = max(min(velocity.x, _active_state.terminal_hor_speed), -_active_state.terminal_hor_speed)
	if abs(control.action_direction.x) < 0.3: # for controller
		velocity.x -= _active_state.deceleration * (velocity.x/_active_state.terminal_hor_speed)
	
	# Limit vertical movement
	velocity.y -= _active_state.drag * (velocity.y/_active_state.terminal_ver_speed)
	if (velocity.y > 0): # only clamp when falling
		velocity.y = max(min(velocity.y, _active_state.terminal_ver_speed), -_active_state.terminal_ver_speed)
	
	return velocity

func update_state(entity):
	# Does the current state still want control?
	var current_priority = 0
	if _active_state != null and _active_state._requests_control(entity):
		current_priority = _active_state.priority
	
	# Check state change
	for component in entity.get_children():
		for state in component.get_children():
			if state is PHYSICS_BASE and state._requests_control(entity) and state.priority > current_priority:
				_active_state = state
				current_priority - state.priority
