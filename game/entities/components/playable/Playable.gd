extends Node

var entity

signal died(entity)

func _entity_ready(entity):
	self.entity = entity

func _die():
	emit_signal("died", entity)
