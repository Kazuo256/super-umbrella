extends Node2D

export(float) var duration = 5
export(float,0,5,0.1) var multiplier = 1.5


var taken=false
var buff_resource = preload("res://entities/collectables/SuperJump/super_jump_buff.tscn")

func _ready():
	$Timer.wait_time = duration

func _on_super_jump_collected(body):
	if not taken:
		#hides pickup
		taken = true
		hide()
		
		if body.has_node("super_jump_buff"):
			body.get_node("super_jump_buff").queue_free()
		
		#creates new buff and adds it to player
		var buff = buff_resource.instance();
		buff.new(multiplier,duration)
		body.add_child(buff)
		
		#start timer
		$Timer.start()


func _on_Timer_timeout():
	taken = false
	show()
