extends Control

export(String, FILE, "*.tscn") var play_scene

func _ready():
	$FadeTransition.fade_in()
	yield($FadeTransition, "fade_completed")
	$Menu/Play.grab_focus()


func _on_Play_pressed():
	$Menu/Play.disabled = true
	for btn in $Menu.get_children():
		btn.focus_mode = FOCUS_NONE
	
	$FadeTransition.fade_out()
	yield($FadeTransition, "fade_completed")
	get_tree().change_scene(play_scene)


func _on_Options_pressed():
	pass # replace with function body


func _on_Quit_pressed():
	get_tree().quit()
